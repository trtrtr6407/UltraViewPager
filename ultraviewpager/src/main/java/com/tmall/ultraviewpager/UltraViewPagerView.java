/*
 *
 *  MIT License
 *
 *  Copyright (c) 2017 Alibaba Group
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all
 *  copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  SOFTWARE.
 *
 */

package com.tmall.ultraviewpager;


import ohos.agp.components.*;
import ohos.agp.window.service.Display;
import ohos.agp.window.service.DisplayAttributes;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;
import ohos.multimodalinput.event.TouchEvent;


/**
 * 自定义滑动组件
 */
public class UltraViewPagerView extends PageSlider implements  UltraViewPagerAdapter.UltraViewPagerCenterListener,  Component.TouchEventListener {

    private UltraViewPagerAdapter pagerAdapter;

    private boolean needsMeasurePage;

    private float multiScrRatio = Float.NaN;
    private boolean enableLoop;
    private boolean autoMeasureHeight;
    private double itemRatio = Double.NaN;
    private int constrainLength;

    private int itemMarginLeft;
    private int itemMarginTop;
    private int itemMarginRight;
    private int itemMarginBottom;

    private float ratio = Float.NaN;

    private UltraViewPager.ScrollMode scrollMode = UltraViewPager.ScrollMode.HORIZONTAL;

    public UltraViewPagerView(Context context) {
        super(context);
        init(context, null);
    }

    public UltraViewPagerView(Context context, AttrSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    private void init(Context context, AttrSet attrs) {

        setClipEnabled(false);
        setReboundEffect(false);
        setTouchEventListener(this);
        onMeasurePage();

    }

    private void onMeasurePage() {

    }



    @Override
    public void postLayout() {
        super.postLayout();
    }


    @Override
    public void setProvider(PageSliderProvider provider) {
        if (provider != null) {
            if (pagerAdapter == null || pagerAdapter.getAdapter() != provider) {
                pagerAdapter = new UltraViewPagerAdapter(provider);
                pagerAdapter.setCenterListener(this);
                pagerAdapter.setEnableLoop(enableLoop);
                pagerAdapter.setMultiScrRatio(multiScrRatio);
                needsMeasurePage = true;
                constrainLength = 0;
                super.setProvider(pagerAdapter);
            }
        } else {
            super.setProvider(provider);
        }
    }



    @Override
    public void setCurrentPage(int itemPos) {
        setCurrentPage(itemPos, false);
    }

    @Override
    public void setCurrentPage(int itemPos, boolean smoothScroll) {
        if (pagerAdapter!=null&&pagerAdapter.getCount() != 0 && pagerAdapter.isEnableLoop()) {
            itemPos = pagerAdapter.getCount() / 2 + itemPos % pagerAdapter.getRealCount();
        }else{
            itemPos=0;
        }

        super.setCurrentPage(itemPos, smoothScroll);
    }

    @Override
    public int getCurrentPage() {
        if (pagerAdapter != null && pagerAdapter.getCount() != 0) {
            int position = super.getCurrentPage();
            return position % pagerAdapter.getRealCount();
        }
        return super.getCurrentPage();
    }



    public int getNextItem() {
        if (pagerAdapter!=null&&pagerAdapter.getCount() != 0) {
            int next = super.getCurrentPage() + 1;
            return next % pagerAdapter.getRealCount();
        }
        return 0;
    }

    /**
     * Set the currently selected page.
     *
     * @param item         Item index to select
     * @param smoothScroll True to smoothly scroll to the new item, false to transition immediately
     */
    public void setCurrentItemFake(int item, boolean smoothScroll) {
        super.setCurrentPage(item, smoothScroll);
    }

    /**
     * Get the currently selected page.
     *
     * @return 当前选中的条目
     */
    public int getCurrentItemFake() {
        return super.getCurrentPage();
    }

    public void setMultiScreen(float ratio) {
        multiScrRatio = ratio;
        if (pagerAdapter != null) {
            pagerAdapter.setMultiScrRatio(ratio);
            needsMeasurePage = true;
        }
        Display display = DisplayManager.getInstance().getDefaultDisplay(getContext()).get();
        DisplayAttributes attributes = display.getAttributes();

        float pageMargin = (1 - ratio) * attributes.width;

    }

    public void setEnableLoop(boolean status) {

        enableLoop = status;
        if (pagerAdapter != null) {
            pagerAdapter.setEnableLoop(enableLoop);
        }
    }

    public void setItemRatio(double itemRatio) {
        this.itemRatio = itemRatio;
    }

    public void setAutoMeasureHeight(boolean autoMeasureHeight) {
        this.autoMeasureHeight = autoMeasureHeight;
    }

    public void setScrollMode(UltraViewPager.ScrollMode scrollMode) {
        this.scrollMode = scrollMode;
        if (scrollMode == UltraViewPager.ScrollMode.VERTICAL){
            setOrientation(DirectionalLayout.VERTICAL);
        }

    }

    public UltraViewPager.ScrollMode getScrollMode() {
        return scrollMode;
    }

    public int getConstrainLength() {
        return constrainLength;
    }

    /**
     * 设置条目之间间距
     *
     * @param left 左间距
     * @param top 上间距
     * @param right 右间距
     * @param bottom 下间距
     */
    public void setItemMargin(int left, int top, int right, int bottom) {
        itemMarginLeft = left;
        itemMarginTop = top;
        itemMarginRight = right;
        itemMarginBottom = bottom;
    }

    public float getRatio() {
        return ratio;
    }

    public void setRatio(float ratio) {
        this.ratio = ratio;
    }

    @Override
    public void center() {
        setCurrentPage(0);
    }

    @Override
    public void resetPosition() {
        setCurrentPage(getCurrentPage());
    }

    private Component swapTouchEvent(Component component) {
        float width = getWidth();
        float height = getHeight();
        float swappedX = (component.getPivotY() / height) * width;
        float swappedY = (component.getPivotX() / width) * height;

        component.setContentPosition(swappedX, swappedY);

        return component;
    }




    @Override
    public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
        if (scrollMode == UltraViewPager.ScrollMode.VERTICAL){
            int width = getWidth();
            int height = getHeight();
            float swappedX = (getPivotY() / height) * width;
            float swappedY = (getPivotX() / width) * height;
            touchEvent.setScreenOffset(swappedX,swappedY);
            return true;
        }
        return false;
    }

    /**
     * 变换类
     */
    public interface PageTransFormer{
        /**
         * 滑动变换方法
         *
         * @param component 组件
         * @param position 索引
         */
        public void transformPage(Component component,float position);
    }

}

