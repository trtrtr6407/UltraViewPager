/*
 *
 *  MIT License
 *
 *  Copyright (c) 2017 Alibaba Group
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all
 *  copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  SOFTWARE.
 *
 */

package com.tmall.ultraviewpager;


import com.tmall.ultraviewpager.utils.LogUtil;
import com.tmall.ultraviewpager.utils.ResUtil;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.PageSlider;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.render.PixelMapHolder;
import ohos.agp.utils.Color;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.window.service.Display;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;
import ohos.media.image.PixelMap;

import java.util.Optional;


public class UltraViewPagerIndicator extends Component implements PageSlider.PageChangedListener, IUltraIndicatorBuilder, Component.DrawTask {
    @Override
    public void onPageSliding(int position, float positionOffset, int positionOffsetPixels) {
        pageOffset = positionOffset;
        invalidate();

        if (pageChangeListener != null) {
            pageChangeListener.onPageSliding(position, positionOffset, positionOffsetPixels);
        }
    }

    @Override
    public void onPageSlideStateChanged(int state) {
        scrollState = state;
        if (pageChangeListener != null) {
            pageChangeListener.onPageSlideStateChanged(state);
        }
    }

    @Override
    public void onPageChosen(int position) {
        if (scrollState == PageSlider.SLIDING_STATE_IDLE) {
            invalidate();
        }

        if (pageChangeListener != null) {
            pageChangeListener.onPageChosen(position);
        }
    }


    interface UltraViewPagerIndicatorListener {
        void build();
    }

    private UltraViewPagerView viewPager;
    private PageSlider.PageChangedListener pageChangeListener;
    private int scrollState;

    //attr for custom
    private int radius;
    private int indicatorPadding;
    private boolean animateIndicator;
    private int gravity;
    private UltraViewPager.Orientation orientation = UltraViewPager.Orientation.HORIZONTAL;

    private int marginLeft;
    private int marginTop;
    private int marginRight;
    private int marginBottom;
    //for circle
    private int focusColor;
    private int normalColor;
    //for custom icon
    private PixelMap focusBitmap;
    private PixelMap normalBitmap;

    //paint
    private Paint paintStroke;
    private Paint paintFill;

    float pageOffset;
    float defaultRadius;

    //default
    private static final int DEFAULT_RADIUS = 3;

    private UltraViewPagerIndicatorListener indicatorBuildListener;

    public UltraViewPagerIndicator(Context context) {
        super(context);
        init();
    }

    public UltraViewPagerIndicator(Context context, AttrSet attrs) {
        super(context, attrs);
        init();
    }

    public UltraViewPagerIndicator(Context context, AttrSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {

        paintStroke = new Paint();
        paintStroke.setStyle(Paint.Style.STROKE_STYLE);
        paintStroke.setAntiAlias(true);
        paintFill = new Paint();
        paintFill.setStyle(Paint.Style.FILL_STYLE);
        paintFill.setAntiAlias(true);

        defaultRadius = dp2px(3);
        addDrawTask(this);
    }


    private float dp2px(int i) {
        Optional<Display> defaultDisplay = DisplayManager.getInstance().getDefaultDisplay(getContext());
        Display display = defaultDisplay.get();
        float scalDensity = display.getAttributes().scalDensity;
        return scalDensity * i;
    }

    public void setViewPager(UltraViewPagerView viewPager) {
        this.viewPager = viewPager;
        this.viewPager.addPageChangedListener(this);
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {

        if (viewPager == null || viewPager.getProvider() == null)
            return;

        final int count = ((UltraViewPagerAdapter) viewPager.getProvider()).getRealCount();
        if (count == 0)
            return;

        int longSize;
        int shortSize;

        int longPaddingBefore;
        int longPaddingAfter;
        int shortPaddingBefore;
        int shortPaddingAfter;
        if (orientation == UltraViewPager.Orientation.HORIZONTAL) {
            longSize = viewPager.getWidth();
            shortSize = viewPager.getHeight();
            longPaddingBefore = getPaddingLeft() + marginLeft;
            longPaddingAfter = getPaddingRight() + marginRight;
            shortPaddingBefore = getPaddingTop() + marginTop;
            shortPaddingAfter = (int) paintStroke.getStrokeWidth() + getPaddingBottom() + marginBottom;
        } else {
            longSize = viewPager.getHeight();
            shortSize = viewPager.getWidth();
            longPaddingBefore = getPaddingTop() + marginTop;
            longPaddingAfter = (int) paintStroke.getStrokeWidth() + getPaddingBottom() + marginBottom;
            shortPaddingBefore = getPaddingLeft() + marginLeft;
            shortPaddingAfter = getPaddingRight() + marginRight;
        }

        final float itemWidth = getItemWidth();
        final int widthRatio = isDrawResIndicator() ? 1 : 2; //bitmap resource X1 : circle  X2
        if (indicatorPadding == 0) {
            indicatorPadding = (int) itemWidth;
        }

        float shortOffset = shortPaddingBefore;
        float longOffset = longPaddingBefore;

        final float indicatorLength = (count - 1) * (itemWidth * widthRatio + indicatorPadding);

        final int horizontalGravityMask = gravity&(LayoutAlignment.HORIZONTAL_CENTER|LayoutAlignment.LEFT|LayoutAlignment.RIGHT);
        final int verticalGravityMask = gravity&(LayoutAlignment.VERTICAL_CENTER|LayoutAlignment.TOP|LayoutAlignment.BOTTOM);

        switch (horizontalGravityMask) {
            case LayoutAlignment.HORIZONTAL_CENTER:
                longOffset = (longSize - longPaddingBefore - longPaddingAfter - indicatorLength) / 2.0f;
                break;
            case LayoutAlignment.RIGHT:
                if (orientation == UltraViewPager.Orientation.HORIZONTAL) {
                    longOffset = longSize - longPaddingAfter - indicatorLength - itemWidth;
                }
                if (orientation == UltraViewPager.Orientation.VERTICAL) {
                    shortOffset = shortSize - shortPaddingAfter - itemWidth;
                }
                break;
            case LayoutAlignment.LEFT:
                longOffset += itemWidth;
            default:
                break;
        }

        switch (verticalGravityMask) {
            case LayoutAlignment.VERTICAL_CENTER:
                shortOffset = (shortSize - shortPaddingAfter - shortPaddingBefore - itemWidth) / 2;
                break;
            case LayoutAlignment.BOTTOM:
                if (orientation == UltraViewPager.Orientation.HORIZONTAL) {
                    shortOffset = shortSize - shortPaddingAfter - getItemHeight();
                }
                if (orientation == UltraViewPager.Orientation.VERTICAL) {
                    longOffset = longSize - longPaddingAfter - indicatorLength;
                }
                break;
            case LayoutAlignment.TOP:
                shortOffset += itemWidth;
            default:
                break;
        }

        if (horizontalGravityMask == LayoutAlignment.HORIZONTAL_CENTER && verticalGravityMask == LayoutAlignment.VERTICAL_CENTER) {
            shortOffset = (shortSize - shortPaddingAfter - shortPaddingBefore - itemWidth) / 2;
        }

        float dX;
        float dY;

        float pageFillRadius = radius;
        if (paintStroke.getStrokeWidth() > 0) {
            pageFillRadius -= paintStroke.getStrokeWidth() / 2.0f;
        }

        //Draw stroked circles
        for (int iLoop = 0; iLoop < count; iLoop++) {
            float drawLong = longOffset + (iLoop * (itemWidth * widthRatio + indicatorPadding));

            if (orientation == UltraViewPager.Orientation.HORIZONTAL) {
                dX = drawLong;
                dY = shortOffset;
            } else {
                dX = shortOffset;
                dY = drawLong;
            }

            if (isDrawResIndicator()) {
                if (iLoop == viewPager.getCurrentPage())
                    continue;
                canvas.drawPixelMapHolder(new PixelMapHolder(normalBitmap), dX, dY, paintFill);
            } else {
                // Only paint fill if not completely transparent
                if (paintFill.getAlpha() > 0) {
                    paintFill.setColor(new Color(normalColor));

                    canvas.drawCircle(dX, dY, pageFillRadius, paintFill);
                }

                // Only paint stroke if a stroke width was non-zero
                if ((int)pageFillRadius != radius) {
                    canvas.drawCircle(dX, dY, radius, paintStroke);
                }
            }
        }

        //Draw the filled circle according to the current scroll
        float cx = (viewPager.getCurrentPage()) * (itemWidth * widthRatio + indicatorPadding);
        if (animateIndicator)
            cx += pageOffset * itemWidth;
        if (orientation == UltraViewPager.Orientation.HORIZONTAL) {
            dX = longOffset + cx;
            dY = shortOffset;
        } else {
            dX = shortOffset;
            dY = longOffset + cx;
        }

        if (isDrawResIndicator()) {
            canvas.drawPixelMapHolder(new PixelMapHolder(focusBitmap), dX, dY, paintStroke);
        } else {
            paintFill.setColor(new Color(focusColor));
            canvas.drawCircle(dX, dY, radius, paintFill);
        }
    }


    private boolean isDrawResIndicator() {
        return focusBitmap != null && normalBitmap != null;
    }

    private float getItemWidth() {
        if (isDrawResIndicator()) {
            return Math.max(focusBitmap.getImageInfo().size.width, normalBitmap.getImageInfo().size.width);
        }
        return radius == 0 ? defaultRadius : radius;
    }

    private float getItemHeight() {
        if (isDrawResIndicator()) {
            return Math.max(focusBitmap.getImageInfo().size.height, normalBitmap.getImageInfo().size.height);
        }
        return radius == 0 ? defaultRadius : radius;
    }


    @Override
    public IUltraIndicatorBuilder setOrientation(UltraViewPager.Orientation orien) {
        this.orientation = orien;
        return this;
    }

    @Override
    public IUltraIndicatorBuilder setRadius(int radius) {
        this.radius = radius;
        return this;
    }

    @Override
    public IUltraIndicatorBuilder setIndicatorPadding(int indicatorPadding) {
        this.indicatorPadding = indicatorPadding;
        return this;
    }

    @Override
    public IUltraIndicatorBuilder setFocusColor(int focusColor) {
        this.focusColor = focusColor;
        return this;
    }

    @Override
    public IUltraIndicatorBuilder setNormalColor(int normalColor) {
        this.normalColor = normalColor;
        return this;
    }

    @Override
    public IUltraIndicatorBuilder setStrokeColor(int strokeColor) {
        paintStroke.setColor(new Color(strokeColor));
        return this;
    }

    @Override
    public IUltraIndicatorBuilder setStrokeWidth(int strokeWidth) {
        paintStroke.setStrokeWidth(strokeWidth);
        return this;
    }

    @Override
    public IUltraIndicatorBuilder setGravity(int gravity) {
        this.gravity = gravity;
        return this;
    }

    @Override
    public IUltraIndicatorBuilder setFocusResId(int focusResId) {
        try {

            focusBitmap = ResUtil.getPixelMap(getContext(), focusResId).get();
        } catch (Exception e) {
            LogUtil.error("UltraViewPager",e.getMessage());
        }
        return this;
    }

    @Override
    public IUltraIndicatorBuilder setNormalResId(int normalResId) {
        try {
            normalBitmap = ResUtil.getPixelMap(getContext(), normalResId).get();
        } catch (Exception e) {
          LogUtil.error("UltraViewPager",e.getMessage());
        }
        return this;
    }

    @Override
    public IUltraIndicatorBuilder setFocusIcon(PixelMap bitmap) {
        focusBitmap = bitmap;
        return this;
    }

    @Override
    public IUltraIndicatorBuilder setNormalIcon(PixelMap bitmap) {
        normalBitmap = bitmap;
        return this;
    }


    @Override
    public IUltraIndicatorBuilder setMargin(int left, int top, int right, int bottom) {
        marginLeft = left;
        marginTop = top;
        marginRight = right;
        marginBottom = bottom;
        return this;
    }

    @Override
    public void build() {
        if (indicatorBuildListener != null) {
            indicatorBuildListener.build();
        }
    }

    public void setPageChangeListener(PageSlider.PageChangedListener pageChangeListener) {
        this.pageChangeListener = pageChangeListener;
    }

    public void setIndicatorBuildListener(UltraViewPagerIndicatorListener listener) {
        this.indicatorBuildListener = listener;
    }
}
