/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tmall.ultraviewpager.slice;

import com.tmall.ultraviewpager.ResourceTable;
import com.tmall.ultraviewpager.UltraPagerAdapter;
import com.tmall.ultraviewpager.UltraPagerMultiAdapter;


import com.tmall.ultraviewpager.UltraViewPager;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;
import ohos.agp.utils.Color;
import ohos.agp.utils.LayoutAlignment;
import ohos.utils.PlainIntArray;

/**
 * 切换的轮播页面
 */
public class PagerSlice extends AbilitySlice implements AbsButton.CheckedStateChangedListener, Component.ClickedListener {
    private String name;
    private int style;
    private UltraViewPager ultraViewPager;
    private UltraPagerAdapter adapter;
    private UltraViewPager.Orientation gravity_indicator;
    private Checkbox loopCheckBox;
    private Checkbox autoScrollCheckBox;
    private Button indicatorBuildBtn;

    private UltraPagerMultiAdapter multiadapter;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        if (intent != null) {
            name = intent.getStringParam("name");
            style = Integer.parseInt(intent.getStringParam("style"));
        }

        setUIContent(ResourceTable.Layout_activity_pager);

        ultraViewPager = (UltraViewPager) findComponentById(ResourceTable.Id_ultra_viewpager);

        switch (style) {
            case 1:
                ultraViewPager.setScrollMode(UltraViewPager.ScrollMode.HORIZONTAL);
                adapter = new UltraPagerAdapter(PagerSlice.this, false);
                ultraViewPager.setProvider(adapter);
                ultraViewPager.setInfiniteRatio(100);
                gravity_indicator = UltraViewPager.Orientation.HORIZONTAL;
                break;
            case 2:
                ultraViewPager.setLayoutConfig(new DependentLayout.LayoutConfig(ComponentContainer.LayoutConfig.MATCH_PARENT,
                        ComponentContainer.LayoutConfig.MATCH_PARENT));
                ultraViewPager.setScrollMode(UltraViewPager.ScrollMode.VERTICAL);

                adapter = new UltraPagerAdapter(PagerSlice.this, false);
                ultraViewPager.setProvider(adapter);
                gravity_indicator = UltraViewPager.Orientation.VERTICAL;
                break;

        }
        initUI();
    }

    private void initUI() {
        Button indicatorStyle = (Button) findComponentById(ResourceTable.Id_indicator);
        Button indicatorGravityHor = (Button) findComponentById(ResourceTable.Id_indicator_gravity_hor);
        Button indicatorGravityVer = (Button) findComponentById(ResourceTable.Id_indicator_gravity_ver);
        Button indicatorGravityVer1 = (Button) findComponentById(ResourceTable.Id_indicator_gravity_ver1);
        Button cancel = (Button) findComponentById(ResourceTable.Id_indicator_build);
        if(style==2){
            indicatorStyle.setText("左中圆");
            indicatorGravityHor.setText("左下圆");
            indicatorGravityVer1.setText("左下图");
            indicatorGravityVer.setText("左中图");
        }

        if (ultraViewPager.getIndicator() == null) {
            ultraViewPager.initIndicator();
            ultraViewPager.getIndicator().setOrientation(gravity_indicator);
        }

        // 默认显示中上圆
        ultraViewPager.getIndicator().setFocusResId(0).setNormalResId(0);
        ultraViewPager.getIndicator().setFocusColor(Color.GREEN.getValue()).setNormalColor(Color.WHITE.getValue())
                .setRadius(10);

        if(ultraViewPager.getIndicator()!=null){
            ultraViewPager.getIndicator().setGravity(LayoutAlignment.HORIZONTAL_CENTER|LayoutAlignment.TOP);
        }

        ultraViewPager.getIndicator().build();

        indicatorStyle.setClickedListener(this);
        indicatorGravityHor.setClickedListener(this);
        indicatorGravityVer.setClickedListener(this);
        indicatorGravityVer1.setClickedListener(this);
        cancel.setClickedListener(this);

        loopCheckBox = (Checkbox) findComponentById(ResourceTable.Id_loop);
        loopCheckBox.setCheckedStateChangedListener(this);

        autoScrollCheckBox = (Checkbox) findComponentById(ResourceTable.Id_autoscroll);
        autoScrollCheckBox.setCheckedStateChangedListener(this);

    }

    @Override
    protected void onActive() {
        super.onActive();
    }

    @Override
    protected void onInactive() {
        super.onInactive();
    }

    @Override
    public void onCheckedChanged(AbsButton buttonView, boolean isChecked) {
        if (buttonView == loopCheckBox) {
            ultraViewPager.setInfiniteLoop(isChecked);
        }
        if (buttonView == autoScrollCheckBox) {
            if (isChecked) {
                PlainIntArray special = new PlainIntArray();
                special.put(0, 5000);
                special.put(1, 1500);
                ultraViewPager.setAutoScroll(2000, special);

            } else{
                ultraViewPager.disableAutoScroll();
            }
        }
    }


    @Override
    public void onClick(Component component) {
        switch (component.getId()) {
            case ResourceTable.Id_indicator:

                ultraViewPager.initIndicator();
                ultraViewPager.getIndicator().setOrientation(gravity_indicator);
                ultraViewPager.getIndicator().setFocusResId(0).setNormalResId(0);
                ultraViewPager.getIndicator().setFocusColor(Color.GREEN.getValue()).setNormalColor(Color.WHITE.getValue())
                        .setRadius(10);
                if(ultraViewPager.getIndicator()!=null){
                    ultraViewPager.getIndicator().setGravity(LayoutAlignment.HORIZONTAL_CENTER|LayoutAlignment.TOP);
                }
                ultraViewPager.getIndicator().build();
                break;
            case ResourceTable.Id_indicator_gravity_hor:

                ultraViewPager.initIndicator();
                ultraViewPager.getIndicator().setOrientation(gravity_indicator);
                ultraViewPager.getIndicator().setFocusResId(0).setNormalResId(0);
                ultraViewPager.getIndicator().setFocusColor(Color.GREEN.getValue()).setNormalColor(Color.WHITE.getValue())
                        .setRadius(10);
                if(ultraViewPager.getIndicator()!=null){
                    ultraViewPager.getIndicator().setGravity(LayoutAlignment.HORIZONTAL_CENTER|LayoutAlignment.BOTTOM);
                }
                ultraViewPager.getIndicator().build();
                break;
            case ResourceTable.Id_indicator_gravity_ver:

                ultraViewPager.initIndicator();
                ultraViewPager.getIndicator().setOrientation(gravity_indicator);

                ultraViewPager.getIndicator().setFocusResId(ResourceTable.Media_tm_biz_lifemaster_indicator_selected).setNormalResId(ResourceTable.Media_tm_biz_lifemaster_indicator_normal);
                if(ultraViewPager.getIndicator()!=null){
                    ultraViewPager.getIndicator().setGravity(LayoutAlignment.HORIZONTAL_CENTER|LayoutAlignment.TOP);
                }
                ultraViewPager.getIndicator().build();
                break;
            case ResourceTable.Id_indicator_gravity_ver1:
                ultraViewPager.initIndicator();
                ultraViewPager.getIndicator().setOrientation(gravity_indicator);

                ultraViewPager.getIndicator().setFocusResId(ResourceTable.Media_tm_biz_lifemaster_indicator_selected).setNormalResId(ResourceTable.Media_tm_biz_lifemaster_indicator_normal);

                if(ultraViewPager.getIndicator()!=null){
                    ultraViewPager.getIndicator().setGravity(LayoutAlignment.HORIZONTAL_CENTER|LayoutAlignment.BOTTOM);
                }
                ultraViewPager.getIndicator().build();
                break;
            case ResourceTable.Id_indicator_build:
                ultraViewPager.disableIndicator();
                break;
        }
    }

}
